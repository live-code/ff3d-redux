import { createSlice } from "@reduxjs/toolkit";
import { addProduct } from '../pages/catalog/store/products/products.actions';
import { increment } from '../pages/order/store/counter.actions';

type ActionType = string | null

const initialState: { error: boolean, actionType: ActionType } = {
  error: false,
  actionType: ''
}

export const errorStore = createSlice({
  name: 'globalError',
  initialState: initialState ,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(addProduct.rejected, (state, action) => {
        state.error = true;
        state.actionType = action.payload as ActionType;
      })
      .addCase(addProduct.fulfilled, (state, action) => {state.error = false })
  }
})

