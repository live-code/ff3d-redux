import React from 'react';
import logo from './logo.svg';
import './App.css';
import { CounterPage } from './pages/order/CounterPage';
import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducer } from './pages/order/store/counter.reducer';
import { configReducer } from './pages/order/store/config.reducer';
import { orderReducer } from './pages/order/store';
import { HomePage } from './pages/home/HomePage';
import { newsStore } from './pages/home/store/news.store';
import { newsFilterStore } from './pages/home/store/news-filter.store';
import { homeReducer } from './pages/home/store';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { productsStore } from './pages/catalog/store/products/products.store';
import { errorStore } from './core/error.store';

const rootReducer = combineReducers({
  order: orderReducer,
  home: homeReducer,
  catalog: productsStore.reducer,
  errorStatus: errorStore.reducer
})

export const store = configureStore({
  reducer: rootReducer,
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
export type AppDispath = typeof store.dispatch;

function App() {
  return (
    <Provider store={store}>
      {/*<CounterPage />*/}
      {/*<HomePage />*/}
      <CatalogPage />
    </Provider>
  );
}

export default App;
