import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Product } from "../../model/product";
import { addProduct } from './products.actions';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    list: [] as Product[],
    error: false
  } ,
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return {
        list: [...action.payload],
        error: false
      };
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.list.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.list.findIndex(p => p.id === action.payload);
      state.list.splice(index, 1)
      state.error = false;
    },
    toggleVisibilitySuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      const product = state.list.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = !product.visibility;
      }
    }
  },
  /*extraReducers: {
    'products/add/rejected': (state, action) => {
      state.error = true;
    },
    'products/add/fulfilled': (state, action) => {
      state.error = false;
    },
  }*/
  extraReducers: builder => {
    builder
      .addCase(addProduct.rejected, (state, action) => {
        state.error = true;
      })
      .addCase(addProduct.fulfilled, (state, action) => {state.error = false })
  }
})

export const {
  getProductsSuccess, addProductSuccess, toggleVisibilitySuccess, deleteProductSuccess,

} = productsStore.actions;
