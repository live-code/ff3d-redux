import axios from "axios";
import { Product } from "../../model/product";
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess,
  toggleVisibilitySuccess
} from './products.store';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { AppThunk } from '../../../../App';

// post
/*
export const addProduct = (product: Omit<Product, 'id' | 'visibility'>) => async (dispatch: any) => {
  try {
    const res = await axios.post<Product>('http://localhost:3001/products', product)
    dispatch(addProductSuccess(res.data));
  } catch (e) {

  }
}
*/

export const addProduct = createAsyncThunk<
  Product,
  Omit<Product, 'id' | 'visibility'>
>(
  'products/add',
  async (payload, { dispatch, rejectWithValue, fulfillWithValue}) =>
  {
    try {
      const res = await axios.post<Product>('http://localhost:3001/products', payload)
      dispatch(addProductSuccess(res.data));
      return res.data as Product;
    } catch (e) {
      return rejectWithValue('products/add')
    }
  }
)


// delete
export const deleteProduct = (id: number) => async (dispatch: any) => {
  try {
    await axios.delete(`http://localhost:3001/products/${id}`)
    dispatch(deleteProductSuccess(id));
  } catch (e) {

  }
}


// toggle
export const toggleProduct = (p: Product) => async (dispatch: any) => {
  try {
    const res = await axios.patch<Product>(`http://localhost:3001/products/${p.id}`, { visibility: !p.visibility})
    dispatch(toggleVisibilitySuccess(res.data));
  } catch (e) {

  }
}


// get
export const getProducts = ():AppThunk => async (dispatch: any) => {
  try {
    const res = await axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(res.data));
  } catch (e) {
  }
}

export const getProducts3 = (): AppThunk => (dispatch: any) => {
    axios.get<Product[]>('http://localhost:3001/products')
      .then(res => {
        dispatch(getProductsSuccess(res.data))
      })
      .catch(err => {

      })
}

export function getProducts2(): AppThunk {
  return function(dispatch: any) {
    axios.get<Product[]>('http://localhost:3001/products')
      .then(res => {
        dispatch(getProductsSuccess(res.data))
      })
      .catch(err => {

      })
  }
}
