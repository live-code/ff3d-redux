import React, { useEffect }  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispath, RootState } from '../../App';
import { addProduct, toggleProduct, deleteProduct, getProducts } from './store/products/products.actions';

export const CatalogPage: React.FC = () => {
  const dispatch = useDispatch() as AppDispath;
  const products = useSelector((state: RootState) => state.catalog.list);
  const error = useSelector((state: RootState) => state.catalog.error);
  const reqStatus = useSelector((state: RootState) => state.errorStatus);

  useEffect(() => {
    dispatch(getProducts())
  }, [])

  function addProductHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 0,
      }))
        .then((res) => console.log(res.payload))
        .catch(err => console.log(err))

    }
  }
  return <div>

    <input
      onKeyDown={addProductHandler}
      type="text" placeholder="product title"/>

    {
      products.map(p => {
        return (
          <li key={p.id}>
            {p.title}

            <div
              className="badge bg-dark p-1"
              onClick={() => dispatch(toggleProduct(p))}
            >
              {p.visibility ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>

            <i className="fa fa-trash" onClick={() => dispatch(deleteProduct(p.id))} />
          </li>
        )
      })
    }
  </div>
};
