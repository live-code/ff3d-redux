import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { setConfig } from './store/config.actions';
import { decrement, increment, reset } from './store/counter.actions';
import { getCounter, getItemsPerPallet, getMaterial, getTotalPallets } from './store/counter.selectors';


export const CounterPage: React.FC = () => {
  const dispatch = useDispatch()
  const qty = useSelector(getCounter)
  const totalPallets = useSelector(getTotalPallets)
  const itemsPerPallet = useSelector(getItemsPerPallet)
  const palletMaterial = useSelector(getMaterial)

  return <div>
    <div>Prodotti Ordinati: {qty}</div>
    <div>
      Total Pallet required:
      {totalPallets} ({itemsPerPallet} items per pallet) - Material {palletMaterial}
    </div>

    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>Set Pallet 5</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 10}))}>Set Pallet 10</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>Set Wood</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>Set Plastic</button>
  </div>
};
