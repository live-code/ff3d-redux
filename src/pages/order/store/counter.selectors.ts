import { RootState } from "../../../App";


export const getCounter = (state: RootState) => state.order.counter;
export const getTotalPallets = (state: RootState) =>
  Math.ceil(state.order.counter / state.order.config.itemsPerPallet)


export const getItemsPerPallet = (state: RootState) => state.order.config.itemsPerPallet;
export const getMaterial = (state: RootState) => state.order.config.material;
