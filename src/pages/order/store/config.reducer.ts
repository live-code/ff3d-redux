import { createReducer } from "@reduxjs/toolkit";
import { Config } from "../model/config";
import { setConfig } from "./config.actions";

const initialState: Config = { itemsPerPallet: 4, material: 'wood', another: 'xyz' };

export const configReducer = createReducer<Config>( initialState,
  builder => builder
    .addCase(setConfig, (state, action) =>  ({ ...state,  ...action.payload}))
)

