import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { decrement, increment, reset } from "./counter.actions";
import { addProduct, deleteProduct } from '../../catalog/store/products/products.actions';
import { deleteProductSuccess } from '../../catalog/store/products/products.store';
/*
export const counterReducer = createReducer(0, {
  [increment.type]: (state, action: PayloadAction<number>) => state + action.payload,
  [decrement.type]: (state, action: PayloadAction<number>) => state - action.payload,
})
*/


export const counterReducer = createReducer(0, builder => {
  builder
    .addCase(increment, (state, action) => state + action.payload)
    .addCase(decrement, (state, action) => state - action.payload)
    .addCase(reset, () => 0)
})
