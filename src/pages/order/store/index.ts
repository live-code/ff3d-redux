import { combineReducers } from "@reduxjs/toolkit";
import { configReducer } from "./config.reducer";
import { counterReducer } from "./counter.reducer";

export const orderReducer = combineReducers({
  counter: counterReducer,
  config: configReducer,
})
