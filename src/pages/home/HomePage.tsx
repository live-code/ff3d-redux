import React, { useState }  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { setFilter } from './store/news-filter.store';
import { getFilteredNews } from './store/news.selectors';
import { addNews, deleteNews, togglePublishing } from './store/news.store';

export type Filter = 'all' | 'published' | 'unpublished';

export const HomePage: React.FC = () => {
  const listFiltered = useSelector(getFilteredNews);
  const dispatch = useDispatch();

  function addNewsHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
    }
  }

  function setFilterHandler(e: React.ChangeEvent<HTMLSelectElement>) {
    dispatch(setFilter(e.currentTarget.value as Filter))
  }

  return <div>
    <input
      onKeyDown={addNewsHandler}
      type="text" placeholder="title news"/>

    <hr/>
    <select onChange={setFilterHandler}>
      <option value="all">Show all</option>
      <option value="published">Published only</option>
      <option value="unpublished">UnPublished only</option>
    </select>
    <hr/>

    {
      listFiltered.map(news => {
        return (
          <li key={news.id}>
            {news.title}

            <div
              className="badge bg-dark p-1"
              onClick={() => dispatch(togglePublishing(news.id))}
            >
              {news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>

            <i className="fa fa-trash" onClick={() => dispatch(deleteNews(news.id))}></i>
          </li>
        )
      })
    }
  </div>
};
