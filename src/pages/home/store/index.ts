import { combineReducers } from "@reduxjs/toolkit";
import { newsFilterStore } from "./news-filter.store";
import { newsStore } from "./news.store";

export const homeReducer = combineReducers({
  news: newsStore.reducer,
  filteredNews: newsFilterStore.reducer
})
