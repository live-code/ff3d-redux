import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../App";
import { Filter } from "../HomePage";
import { News } from "../model/news";

export const newsStore = createSlice({
  name: 'news',
  initialState: [] as News[],
  reducers: {
    addNews(state: News[], action: PayloadAction<string>) {
      state.push({ id: Date.now(), title: action.payload, published: false})
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.findIndex(news => news.id === action.payload)
      if (index !== -1) {
        state.splice(index, 1);
      }
    },
    togglePublishing(state, action: PayloadAction<number>) {
      const news = state.find(news => news.id === action.payload);
      if (news) {
        news.published = !news.published
      }
    }
  }
})

export const { addNews, deleteNews, togglePublishing } = newsStore.actions
