import { RootState } from "../../../App";
import { Filter } from "../HomePage";

export const getNews = (state: RootState) => state.home.news;
export const getFilteredNews = (state: RootState) => {
  switch (state.home.filteredNews) {
    case 'published': return state.home.news.filter(n => n.published)
    case 'unpublished': return state.home.news.filter(n => !n.published)
    case 'all': return state.home.news
  }
}
