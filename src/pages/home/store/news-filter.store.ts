import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../App";
import { Filter } from "../HomePage";
import { News } from "../model/news";


export const newsFilterStore = createSlice({
  name: 'newsFilter',
  initialState: 'all' as Filter,
  reducers: {
    setFilter(state, action: PayloadAction<Filter>) {
      // state = action.payload
      return action.payload;
    }
  }
})

export const { setFilter } = newsFilterStore.actions
